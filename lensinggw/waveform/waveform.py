import numpy as np
import sys
from optparse import OptionParser
import configparser as ConfigParser
from lensinggw.waveform.waveform_utils import waveform_model
from lensinggw.utils.utils import get_lensed_gws
from lensinggw.amplification_factor.amplification_factor import geometricalOpticsMagnification


class gw_signal(object):
    """
    Class to compute lensed and unlensed GW signals and signal-to-noise-ratios
    
    :param config_file: configuration file containing the binary parameters and waveform specifications
    :type config_file: INI file
        
    Example
    -------
    signal_model = gw_signal(*config_file*)
    """

    def __init__(self, config_file):
        
        self.config_file = config_file
        self.initialize()

    def initialize(self):
        """
        Initializes the waveform model through the parameters specified in `config_file`
        
        Raises
        ------
        StandardError
            If the waveforms in a detector are requested but its power spectral density is not specified
        """
        
        # read the config file 
        Config = ConfigParser.ConfigParser()
        Config.optionxform = str
        Config.read(self.config_file)
        ConfigParser.allow_no_value=True 
        
        injection_parameters = {}
        waveform_parameters  = {}
        detectors            = {}
        
        # parse every input individually
        for (each_key, each_val) in Config.items('injection parameters'):
            injection_parameters.update({each_key: eval(each_val)})
                
        for (each_key, each_val) in Config.items('waveform input'):
            try: value = eval(each_val)
            except: value = each_val
            waveform_parameters.update({each_key: value})
            
        for (each_key, each_val) in Config.items('detectors'):
            try:
                each_val = Config.getboolean('detectors', each_key)
                if each_val:  
                    try:
                        psd_path = Config.get('detectors', each_key+'-psd')
                        if psd_path == '':
                            sys.stderr.write('\n\nMust specify a psd file for %s\n' % each_key)
                            sys.stderr.write('Aborting\n')
                            exit(-1)
                        else:
                            detectors.update({each_key: psd_path})
                    except:
                        sys.stderr.write('\n\nMust specify a psd file for %s\n' % each_key)
                        sys.stderr.write('Aborting\n')
                        exit(-1)
            except: pass
        
        # print them on screen
        sys.stdout.write('\n---- Will simulate a signal with the following parameters ----\n\n')
        for key, value in injection_parameters.items():
            sys.stdout.write("{0} --> {1}\n".format(key.ljust(len('logdistance')),value))
        sys.stdout.write('\n')
        
        sys.stdout.write('\n---- Will use the following setup ----\n\n')
        for key, value in waveform_parameters.items():
            sys.stdout.write("{0} --> {1}\n".format(key.ljust(len('sampling-rate')),value))
        sys.stdout.write('\n')
        
        sys.stdout.write('\n---- Will simulate the signal in the following detectors ----\n\n')
        for key, value in detectors.items():
            sys.stdout.write("{0} --> {1}\n".format(key.ljust(len('H1-psd')),True))
            sys.stdout.write("{0}-psd --> {1}\n".format(key.ljust(len('H1')),value))
        sys.stdout.write('\n')

        # instantiate the waveform_model class
        self.WaveformModel = waveform_model(injection_parameters, waveform_parameters, detectors)  
    
    def unlensed_gw(self):
        """
        Computes unlensed polarizations, strains and their frequencies
        
        :return: unlensed :math:`f, \, \\tilde{h}_+(f), \, \\tilde{h}_{\\times}(f), \, s = \{\'det_i \, \': s^{det_i}\}`
        
            where :math:`s^{det} =\\tilde{h}_+(f)F^{det}_+ + \\tilde{h}_{\\times}(f)F^{det}_{\\times}` is the unlensed strain in the detector :math:`det`       
        :rtype: array, array, array, dict     
        """
        
        freqs, hp_tilde, hc_tilde = self.WaveformModel.plain_template()  
        strain_dict               = self.WaveformModel.strain()   
        return freqs, hp_tilde, hc_tilde, strain_dict
    
    def unlensed_snr(self):
        """
        Computes the signal-to-noise ratio of the unlensed strains 
        
        :return: signal-to-noise-ratios :math:`SNR = \{\'det_i \, \': SNR^{det_i}\}` of the unlensed strains in each detector. Stores the unlensed :math:`SNR` of the global network at the key :math:`\'network\'`
        :rtype: dict
        """
        
        SNR_dict = self.WaveformModel.SNR()
        for (each_key,each_val) in SNR_dict.items():
            if each_key == 'network':
                sys.stdout.write('\nNetwork SNR of the unlensed simulation {0:f}\n\n'.format(SNR_dict['network']))
            else:
                sys.stdout.write("\nUnlensed SNR in %s = %.2f"%(each_key,each_val))
        return SNR_dict
    
    
    def lensed_gw(self,
                  Img_ra,
                  Img_dec,
                  source_pos_x,
                  source_pos_y,
                  zL,
                  zS,
                  lens_model_list,
                  kwargs_lens_list,
                  diff         = None,
                  scaled       = False,
                  scale_factor = None,
                  cosmo        = None):
        """
        Computes lensed polarizations, strains and their frequencies
        
        :param Img_ra: images right ascensions (arbitrary units)
        :type Img_ra: indexable object
        :param Img_dec: images declinations (arbitrary units)
        :type Img_dec: indexable object
        :param source_pos_x: source right ascension (arbitrary units)
        :type source_pos_x: float
        :param source_pos_y: source declination (arbitrary units)
        :type source_pos_y: float
        :param zL: lens redshift
        :type zL: float
        :param zS: source redshift
        :type zS: float
        :param lens_model_list: names of the lens profiles to be considered for the lens model
        :type lens_model_list: list of strings
        :param kwargs_lens_list: keyword arguments of the lens parameters matching each lens profile in *lens_model_list*
        :type kwargs_lens_list: list of dictionaries
        :param diff: step for numerical differentiation, *optional*. Only needed for potentials that require numerical differentiation. If not specified, analytical differentiation is assumed
        :type diff: float
        :param scaled: specifies if the input is given in arbitrary units, *optional*. If not specified, the input is assumed to be in radians
        :type scaled: bool
        :param scale_factor: scale factor, *optional*. Used to account for the proper conversion factor in the time delays when coordinates are given in arbitrary units, as per :math:`x_{a.u.} = x_{radians}/scale\_factor`. Only considered when *scaled* is *True*
        :type scale_factor: float
        :param cosmo: cosmology used to compute angular diameter distances, *optional*. If not specified, a :math:`\\mathrm{FlatLambdaCDM}` instance with :math:`H_0=69.7, \Omega_0=0.306, T_{cmb0}=2.725` is considered
        :type cosmo: instance of the astropy cosmology class
        
        :return: lensed :math:`f, \, \\tilde{h}_+(f), \, \\tilde{h}_{\\times}(f), \, s = \{\'det_i \, \': s^{det_i}\}`
        
            where :math:`s^{det} =\\tilde{h}_+(f)F^{det}_+ + \\tilde{h}_{\\times}(f)F^{det}_{\\times}` is the lensed strain in the detector :math:`det`
        :rtype: array, array, array, dict
        """

        # compute the unlensed signals and SNRs
        freqs, hp_tilde, hc_tilde = self.WaveformModel.plain_template()  
        strain_dict               = self.WaveformModel.strain() 

        # compute the amplification factor from the images and lens potential
        F = geometricalOpticsMagnification(freqs,
                                           Img_ra,Img_dec,
                                           source_pos_x,source_pos_y,
                                           zL,zS,
                                           lens_model_list,
                                           kwargs_lens_list,
                                           diff         = diff,
                                           scaled       = scaled,
                                           scale_factor = scale_factor,
                                           cosmo        = cosmo)

        # build the lensed signals
        hp_tilde_lensed, hc_tilde_lensed, lensed_strain_dict = get_lensed_gws(F, hp_tilde, hc_tilde, strain_dict) 
            
        return freqs, hp_tilde_lensed, hc_tilde_lensed, lensed_strain_dict
        
    def lensed_snr(self,
                   Img_ra,
                   Img_dec,
                   source_pos_x,
                   source_pos_y,
                   zL,
                   zS,
                   lens_model_list,
                   kwargs_lens_list,
                   diff         = None,
                   scaled       = False,
                   scale_factor = None,
                   cosmo        = None):
                   
        """
        Computes signal-to-noise ratios of the lensed strains 
        
        :param Img_ra: images right ascensions (arbitrary units)
        :type Img_ra: indexable object
        :param Img_dec: images declinations (arbitrary units)
        :type Img_dec: indexable object
        :param source_pos_x: source right ascension (arbitrary units)
        :type source_pos_x: float
        :param source_pos_y: source declination (arbitrary units)
        :type source_pos_y: float
        :param zL: lens redshift
        :type zL: float
        :param zS: source redshift
        :type zS: float
        :param lens_model_list: names of the lens profiles to be considered for the lens model
        :type lens_model_list: list of strings
        :param kwargs_lens_list: keyword arguments of the lens parameters matching each lens profile in *lens_model_list*
        :type kwargs_lens_list: list of dictionaries
        :param diff: step for numerical differentiation, *optional*. Only needed for potentials that require numerical differentiation. If not specified, analytical differentiation is assumed
        :type diff: float
        :param scaled: specifies if the input is given in arbitrary units, *optional*. If not specified, the input is assumed to be in radians
        :type scaled: bool
        :param scale_factor: scale factor, *optional*. Used to account for the proper conversion factor in the time delays when coordinates are given in arbitrary units, as per :math:`x_{a.u.} = x_{radians}/scale\_factor`. Only considered when *scaled* is *True*
        :type scale_factor: float
        :param cosmo: cosmology used to compute angular diameter distances, *optional*. If not specified, a :math:`\\mathrm{FlatLambdaCDM}` instance with :math:`H_0=69.7, \Omega_0=0.306, T_{cmb0}=2.725` is considered
        :type cosmo: instance of the astropy cosmology class
     
        :return: signal-to-noise-ratios :math:`SNR = \{\'det_i \, \': SNR^{det_i}\}` of the lensed strains in each detector. Stores the lensed :math:`SNR` of the global network at the key :math:`\'network\'`
        :rtype: dict
        """
        
        # initialize the keyword arguments to compute the lensed signals
        kwargs = {'Img_ra'          : Img_ra,
                  'Img_dec'         : Img_dec,
                  'source_pos_x'    : source_pos_x,
                  'source_pos_y'    : source_pos_y,
                  'zL'              : zL,
                  'zS'              : zS,
                  'lens_model_list' : lens_model_list,
                  'kwargs_lens_list': kwargs_lens_list,
                  'diff'            : diff,
                  'scaled'          : scaled,
                  'scale_factor'    : scale_factor,
                  'cosmo'           : cosmo}
        
        # compute the lensed SNRs
        lensed_SNR_dict = self.WaveformModel.SNR(lensed = True, **kwargs)
        
        # print them out
        for (each_key,each_val) in lensed_SNR_dict.items():
            if each_key == 'network':
                sys.stdout.write('\nNetwork SNR of the lensed simulation {0:f}\n\n'.format(lensed_SNR_dict['network']))
            else:
                sys.stdout.write("\nLensed SNR in %s = %.2f"%(each_key,each_val))
        return lensed_SNR_dict    
