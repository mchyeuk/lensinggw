Multi-component macromodel
==========================
* `Example 3 - Solve the lens model (multi-component macromodel) <https://gitlab.com/gpagano/lensinggw/-/blob/master/lensinggw/examples/binary_point_mass_only_macromodel.py>`__

| By default, ``lensingGW`` considers the first profile in *lens_model_list* as macromodel. However, users can
  indicate the profiles that form the macromodel by specifying the matching indices in the lens list.
  This allows to indicate a different profile or multi-component macromodels.
| For example, we can define a 2-component macromodel constituted by both the point lenses of the `previous examples <https://gpagano.gitlab.io/lensinggw/usage.html#solve-the-lens-model>`_ by adding the following specification in the solver settings

.. code-block:: python

        solver_settings.update({'MacroIndex' : [0,1]})
        
If the number of specified indices corresponds to the number of lens profiles, all the deflectors are considered
as part of the macromodel and only the macroimages are investigated.