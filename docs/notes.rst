Technical notes
---------------

``lensingGW`` has a modular structure which allows to import each routine individually.
Detailed package description is available at the `lensingGW package <https://gpagano.gitlab.io/lensinggw/lensinggw.html>`_
section of the documentation. Here, we provide a few technical notes on the packages that require special attention.

The amplification factor package
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


This package computes the geometrical optics amplification from a set of images and lensing potentials.

Unlike in the light lensing, gravitational waves from compact binaries are transient signals. Thus, only the images
that superimpose into the detectors should be fed to the amplification routine. This amounts to considering groups of
images whose pairwise time delays are shorter than the unlensed signal duration.
The time delay of each image can be checked through the ``TimeDelay`` module in the ``utils`` package.

The solver package
~~~~~~~~~~~~~~~~~~

| The solver package finds the image positions of lensed compact sources by specifying a number of settings. It implements the 
  *two-step procedure* presented in `Pagano et al. 2020 <https://arxiv.org/abs/2006.12879>`_: the minimal solver setup requires to specify the extension of the
  search windows for the macromodel and for the complete model. However, each part of the iterative procedure is customizable by 
  enabling the :math:`\texttt{optimization}` mode.
| Tuning of the solver settings is also recommended when no images are found. As the number of pixels and grid size of each 
  iteration determine how well the lens potential is resolved, users may have to adapt them to the specific model under consideration.

This section contains the complete list of solver settings that can be tuned by the users along with their descriptions and
default values. For the sake of clarity, here they are grouped in:
  
  + *standard settings* -- specifications that are always considered for the *two-step procedure*. If users don't
    specify input for these settings, default values are assumed
      
  + *optimization-related settings* -- settings that are considered only when the :math:`\texttt{optimization}` mode is active.
    Users who wish to use these settings should enable the :math:`\texttt{optimization}` mode.
     
  + *further specifications* -- additional features.
   
| The settings are specified through a dictionary of keywords arguments, as shown in the
  `solver usage examples <https://gpagano.gitlab.io/lensinggw/usage.html#solve-the-lens-model>`_.
 
**Standard settings**

For most cases, the :math:`\texttt{optimization}` mode is not required.
The following settings apply the solving algorithm in its standard form.

+-------------------+-------------+--------------------------+-----------------------------------------------------------------+
| Option            | Type        | Default                  | Description                                                     |
+===================+=============+==========================+=================================================================+
| Scaled            | *bool*      | False                    | | Specifies if the input is given in arbitrary units. If        |
|                   |             |                          | | :math:`False`, radians are assumed                            |
+-------------------+-------------+--------------------------+-----------------------------------------------------------------+
| ScaleFactor       | *float*     |  1                       | | Constant factor :math:`\alpha` to convert from radians to     |
|                   |             |                          | | arbitrary units, as per :math:`x_{a.u.} = x_{radians}/\alpha` |            
+-------------------+-------------+--------------------------+-----------------------------------------------------------------+
| MacroIndex        | *list*      | [0]                      | | Indices matching the profiles in                              |
|                   |             |                          | | *lens_model_list* that form the macromodel                    |       
+-------------------+-------------+--------------------------+-----------------------------------------------------------------+
| SearchWindowMacro | *float*     |   needs user input       | Size of the first macromodel grid                               |
+-------------------+-------------+--------------------------+-----------------------------------------------------------------+
| PixelsMacro       | *int*       | :math:`10^3`             | | Number of pixels of the first macromodel                      |
|                   |             |                          | | grid                                                          |       
+-------------------+-------------+--------------------------+-----------------------------------------------------------------+
| OverlapDistMacro  | *float*     | :math:`10^{-15}\rm{rad}` | | Distance below which *macroimages* are                        |
|                   |             |                          | | considered overlaps                                           |       
+-------------------+-------------+--------------------------+-----------------------------------------------------------------+
| ImgIndex          | | *list*    | *None*                   | | Indices of the macroimages to be considered for               |
|                   | | or        |                          | | the *two-step procedure*. If *None*,                          |       
|                   | | *None*    |                          | | all macroimages are considered                                |
+-------------------+-------------+--------------------------+-----------------------------------------------------------------+
| SearchWindow      | *float*     |   needs user input       | | Same as *SearchWindowMacro*, but for                          |
|                   |             |                          | | the complete model                                            |       
+-------------------+-------------+--------------------------+-----------------------------------------------------------------+
| Pixels            | *int*       | :math:`10^3`             | | Same as *PixelsMacro*, but for                                |       
|                   |             |                          | | the complete model                                            |  
+-------------------+-------------+--------------------------+-----------------------------------------------------------------+
| OverlapDist       | *float*     | :math:`10^{-15}\rm{rad}` | | Same as *OverlapDistMacro*, but for                           |       
|                   |             |                          | | the complete model                                            |  
+-------------------+-------------+--------------------------+-----------------------------------------------------------------+
| PrecisionLimit    | *float*     | :math:`10^{-20}\rm{rad}` | | Precision of the solutions in the                             |
|                   |             |                          | | source plane                                                  |       
+-------------------+-------------+--------------------------+-----------------------------------------------------------------+
| Optimization      | *bool*      | False                    | Enables the :math:`\texttt{optimization}` mode                  |       
+-------------------+-------------+--------------------------+-----------------------------------------------------------------+
| Verbose           | *bool*      | False                    | Prints detailed diagnostic                                      |       
+-------------------+-------------+--------------------------+-----------------------------------------------------------------+

| Entries for *SearchWindow* and *SearchWindowMacro* must necessarily be specified by the user.
| Additional tuning is possible through the :math:`\texttt{optimization}` mode, which is particularly useful in non-converging cases. 

**Optimization-related settings**

When the :math:`\texttt{optimization}` mode is active, users can tune the following additional parameters

+---------------------------------+-----------+--------------------------+--------------------------------------------------------------------+
| Option                          | Type      | Default                  | Description                                                        |
+=================================+===========+==========================+====================================================================+
| OptimizationWindowMacro         | *float*   | 2                        | | Multiplying factor :math:`\gamma` for the size                   |
|                                 |           |                          | | of the macromodel iteration grids.                               |
|                                 |           |                          | | The next window size :math:`w_s` is                              |
|                                 |           |                          | | :math:`w_s = \gamma p_s`, where :math:`p_s` is the pixel size    |
|                                 |           |                          | | of the current iteration                                         |
+---------------------------------+-----------+--------------------------+--------------------------------------------------------------------+
| OptimizationPixelsMacro         | *int*     | :math:`30`               | | Number of pixels of the macromodel                               |
|                                 |           |                          | | iteration grids                                                  |
+---------------------------------+-----------+--------------------------+--------------------------------------------------------------------+
| MinDistMacro                    | | *float* |                          | | Minimum ray-shooted distance                                     |
|                                 | | or      | *None*                   | | from the source :math:`d^s_0` that pixels of the                 |
|                                 | | *None*  |                          | | first macromodel grid must satisfy                               |
|                                 |           |                          | | to be iterated over.                                             | 
|                                 |           |                          | | If *None*, all candidate pixels                                  |
|                                 |           |                          | | are iterated over                                                |
+---------------------------------+-----------+--------------------------+--------------------------------------------------------------------+
| ImprovementMacro                | | *float* |                          | | Improvement :math:`\delta` on the ray-shooted                    |
|                                 | | or      | *None*                   | | distance :math:`d^s_0` that pixels of the                        |
|                                 | | *None*  |                          | | macromodel grids must satisfy to be                              |
|                                 |           |                          | | iterated over.                                                   |
|                                 |           |                          | | Pixels are considered for the :math:`n^{th}`                     |
|                                 |           |                          | | iteration if their ray-shooted                                   |
|                                 |           |                          | | distance at the :math:`{n-1}^{th}` iteration                     |
|                                 |           |                          | | is :math:`d^s_{n-1} < d^s_0 \cdot \delta^{n-1}`,                 |
|                                 |           |                          | | where :math:`0 < \delta \leq1`.                                  |
|                                 |           |                          | | If *None*, :math:`d^s_{n-1}\equiv d^s_0`                         |
|                                 |           |                          | | for all the iterations                                           |
+---------------------------------+-----------+--------------------------+--------------------------------------------------------------------+
| OptimizationPrecisionLimitMacro | *float*   | :math:`10^{-20}\rm{rad}` | | Precision of the macromodel                                      |
|                                 |           |                          | | solutions in the source plane                                    |
+---------------------------------+-----------+--------------------------+--------------------------------------------------------------------+
| OptimizationWindow              | *float*   | 2                        | | Same as **OptimizationWindowMacro**,                             |
|                                 |           |                          | | but for the complete model                                       |
+---------------------------------+-----------+--------------------------+--------------------------------------------------------------------+
| OptimizationPixels              | *int*     | :math:`30`               | | Same as **OptimizationPixelsMacro**,                             |
|                                 |           |                          | | but for the complete model                                       |
+---------------------------------+-----------+--------------------------+--------------------------------------------------------------------+
| MinDist                         | | *float* |                          | | Same as **MinDistMacro**, but for the                            |
|                                 | | or      | *None*                   | | complete model                                                   |
|                                 | | *None*  |                          |                                                                    |
+---------------------------------+-----------+--------------------------+--------------------------------------------------------------------+
| Improvement                     | | *float* |                          | | Same as **ImprovementMacro**,                                    | 
|                                 | | or      | *None*                   | | but for the complete model                                       |
|                                 | | *None*  |                          |                                                                    |
+---------------------------------+-----------+--------------------------+--------------------------------------------------------------------+
| OptimizationPrecisionLimit      | *float*   | :math:`10^{-20}\rm{rad}` | | Same as                                                          |
|                                 |           |                          | | **OptimizationPrecisionLimitMacro**,                             |
|                                 |           |                          | | but for the complete model                                       |
+---------------------------------+-----------+--------------------------+--------------------------------------------------------------------+

**further specifications**

In addition to the above-mentioned options, users can specify

+------------------------+-----------+------------------+---------------------------------------------------+
| Option                 | Type      | Default          | Description                                       |
+========================+===========+==================+===================================================+
| OnlyMacro              | *bool*    | False            | Solves for the macromodel only                    | 
+------------------------+-----------+------------------+---------------------------------------------------+ 
| NearSource             | *bool*    | False            | | Enables a further screening for                 |
|                        |           |                  | | macroimages close to the unlensed               |
|                        |           |                  | | image position, with a customized               |
|                        |           |                  | | window size and number of pixels                |
+------------------------+-----------+------------------+---------------------------------------------------+
| SearchWindowNearSource | | *float* |                  | | Same as *SearchWindowMacro*, but for            |
|                        | | or      | needs user input | | the inspection near the unlensed                | 
|                        | | *None*  |                  | | image position.                                 |
|                        |           |                  | | It requires :math:`\texttt{NearSource}= True`   |
+------------------------+-----------+------------------+---------------------------------------------------+
| PixelsNearSource       | *int*     | :math:`10^3`     | | Same as *PixelsMacro*, but for                  |       
|                        |           |                  | | the inspection near the unlensed                | 
|                        |           |                  | | image position.                                 |
|                        |           |                  | | It requires :math:`\texttt{NearSource}= True`   |
+------------------------+-----------+------------------+---------------------------------------------------+


The :math:`\texttt{NearSource}` option is compatible with the :math:`\texttt{optimization}` mode.
When both are active, the :math:`\texttt{optimization}` settings applied to the screening near the unlensed image position
are the same as the ones specified for the macromodel. The *SearchWindowNearSource* must necessarily be specified 
when this option is active.